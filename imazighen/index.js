var net = require("net");
var JSONStream = require('JSONStream');
var messages = require("./messages");
var brain = require("./brain")(messages);

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var botId = {
  name: botName,
  key: botKey
};

client = net.connect(serverPort, serverHost, function() {
  return send(messages.join(botId));
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  send(brain.process(data));
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
