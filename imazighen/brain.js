function throttle(pieces, myCar) {
  var currentPieceIndex = myCar.piecePosition.pieceIndex;
  var nextPieceIndex = (currentPieceIndex < pieces.length - 1) ? currentPieceIndex + 1 : 0;
  if (pieces[currentPieceIndex]) {
    if (pieces[nextPieceIndex]) {
      return 1.0;
    } else {
      return 0.1;
    }
  } else {
    return 0.5;
  }
}

function status(myCar, pieces) {
  console.log("BROOM: "
    + myCar.piecePosition.pieceIndex + "/" + pieces.length
    + "; " + myCar.piecePosition.inPieceDistance
    + "; lap: " + myCar.piecePosition.lap
    + "; angle: " + myCar.angle);
}

module.exports = function(messages) {
  var pieces = [];
  var canMove = true;
  var myColor = null;
  return {
    process: function(data) {
      switch(data.msgType) {
        case "join":
          console.log("received join");
          break;
        case "gameStart":
          console.log("received gameStart");
          break;
        case "gameEnd":
          console.log("received gameEnd");
          break;
        case "gameInit":
          console.log("received gameInit");
          pieces = data.data.race.track.pieces.map(function(piece) {
            if (piece.length) {
              return true;
            } else {
              return false;
            }
          });
          break;
        case "yourCar":
          console.log("received yourCar");
          break;
        case "spawn":
          console.log("received spawn");
          break;
        case "crash":
          console.log("received crash");
          break;
        case "carPositions":
          console.log("received carPositions");
          var myCarData = data.data[0];
          if (canMove) {
            status(myCarData, pieces);
            return messages.throttle(throttle(pieces, myCarData));
          }
        default:
          console.log("uncatched message: " + data.msgType);
      }
      return messages.ping();
    }
  };
}
