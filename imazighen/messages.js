exports.join = function(botId) {
  return {
    msgType: "join",
    data: botId
  };
}

exports.joinRace = function(botId, track, cars) {
  console.log("joinRace: " + track + " with " + cars + " cars")
  return {
    msgType: "joinRace",
    data: {
      botId: botId,
      trackName: track,
      carCount: cars
    }
  };
}

exports.ping = function () {
  return {
    msgType: "ping",
    data: {}
  };
};

exports.throttle = function (amount) {
  console.log("throttle: " + amount);
  return {
    msgType: "throttle",
    data: amount
  };
};

exports.turbo = function () {
  console.log("turbo activated!");
  return {
    msgType: "turbo",
    data: "TURBO BOOST!!!"
  };
};

exports.switchLane = function (direction) {
  console.log("switch lane to " + direction);
  return {
    msgType: "switchLane",
    data: direction
  };
};
